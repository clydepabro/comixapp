namespace CRUDcomics.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initializeDB : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ComicDetails",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        comicName = c.String(),
                        comicPublisher = c.String(),
                        comicVolume = c.Int(nullable: false),
                        comicIssueNumber = c.Int(nullable: false),
                        datePublished = c.DateTime(nullable: false),
                        comicPrice = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ComicDetails");
        }
    }
}
