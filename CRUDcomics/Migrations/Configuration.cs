namespace CRUDcomics.Migrations
{
    using CRUDcomics.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<CRUDcomics.Models.ComicDBContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(CRUDcomics.Models.ComicDBContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
            IList<ComicDetail> defaultDetails = new List<ComicDetail>();

            defaultDetails.Add( new ComicDetail() { comicName = "Incredible Hulk", comicPublisher = "Marvel", comicVolume = 1, comicIssueNumber = 180, datePublished = new DateTime(1974,10,10), comicPrice = 549 });
            defaultDetails.Add(new ComicDetail() { comicName = "Incredible Hulk", comicPublisher = "Marvel", comicVolume = 1, comicIssueNumber = 181, datePublished = new DateTime(1974, 11, 1), comicPrice = 1503 });
            defaultDetails.Add(new ComicDetail() { comicName = "Spiderman", comicPublisher = "Marvel", comicVolume = 1, comicIssueNumber = 300, datePublished = new DateTime(1988, 05, 01), comicPrice = 150 });

            foreach (ComicDetail comic in defaultDetails)
                context.ComicDetails.Add(comic);

            base.Seed(context);

        }
    }
}
