﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRUDcomics.Models
{
    class ComicDetail
    {
        [Key]
        public int ID { get; set; }
        public String comicName { get; set; }
        public String comicPublisher { get; set; }
        public int comicVolume { get; set; }
        public int comicIssueNumber { get; set; }
        public DateTime datePublished { get; set; }
        public int comicPrice { get; set; }
    }
}
