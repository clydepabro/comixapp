﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRUDcomics.Models
{
    class ComicDBContext : DbContext
    {
        public ComicDBContext() : base("MyDbConnectionString")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<ComicDBContext, Migrations.Configuration>("MyDbConnectionString"));
        }

        public DbSet <ComicDetail> ComicDetails { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}
