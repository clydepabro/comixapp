namespace CRUDcomics.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ComicDetail
    {
        public int ID { get; set; }

        public string comicName { get; set; }

        public string comicPublisher { get; set; }

        public int comicVolume { get; set; }

        public int comicIssueNumber { get; set; }

        public DateTime datePublished { get; set; }

        public int comicPrice { get; set; }
    }
}
