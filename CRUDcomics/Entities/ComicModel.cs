namespace CRUDcomics.Entities
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class ComicModel : DbContext
    {
        public ComicModel()
            : base("MyDbConnectionString")
        {
        }

        public virtual DbSet<ComicDetail> ComicDetails { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
