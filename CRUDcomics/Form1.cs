﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Entity;
using CRUDcomics.Entities;

namespace CRUDcomics
{
    public partial class Form1 : Form
    {

        ComicDetail MyDetail = new ComicDetail();

        public Form1()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView1.CurrentRow.Index != -1)
            {
                MyDetail.ID = Convert.ToInt32(dataGridView1.CurrentRow.Cells[0].Value);
                using (var MyDBEntities = new ComicModel())
                {
                    MyDetail = MyDBEntities.ComicDetails.Where(x => x.ID == MyDetail.ID).FirstOrDefault();
                    txtComicName.Text = MyDetail.comicName;
                    txtComicPublisher.Text = MyDetail.comicPublisher;
                    txtComicVolume.Text = MyDetail.comicVolume.ToString();
                    txtComicIssueNumber.Text = MyDetail.comicIssueNumber.ToString();
                    txtDatePublished.Text = MyDetail.datePublished.ToString();
                    txtComicPrice.Text = MyDetail.comicPrice.ToString();


                    btnSave.Text = "Update";
                    btnSave.BackColor = Color.Red;

                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            PopGridView();
        }

        private void PopGridView()
        {
            using (var MyModelEntities = new ComicModel()) {
                dataGridView1.DataSource = MyModelEntities.ComicDetails.ToList<ComicDetail>();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {

            MyDetail.comicName = txtComicName.Text;
            MyDetail.comicPublisher = txtComicPublisher.Text;
            MyDetail.comicVolume = Convert.ToInt32(txtComicVolume.Text);
            MyDetail.comicIssueNumber = Convert.ToInt32(txtComicIssueNumber.Text);
            MyDetail.datePublished = Convert.ToDateTime(txtDatePublished.Text);
            MyDetail.comicPrice = Convert.ToInt32(txtComicPrice.Text);

            using (var MyDBEntities = new ComicModel())
            {
                if (MyDetail.ID == 0) {
                    MyDBEntities.ComicDetails.Add(MyDetail);
                    MyDBEntities.SaveChanges();

                    MessageBox.Show("Information has been saved.", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else {
                    MyDBEntities.Entry(MyDetail).State = System.Data.Entity.EntityState.Modified;
                    MyDBEntities.SaveChanges();

                    MyDetail.ID = 0;
                    btnSave.Text = "Save";
                    btnSave.BackColor = Color.Silver;

                    ClearFields();
                }

      
            }
            PopGridView();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to delete this information?", "Please Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.OK) {
                using (var MyDBEntities = new ComicModel())
                {
                    var entry = MyDBEntities.Entry(MyDetail);
                    if (entry.State == EntityState.Detached)
                    {
                        MyDBEntities.ComicDetails.Attach(MyDetail);
                        MyDBEntities.ComicDetails.Remove(MyDetail);
                        MyDBEntities.SaveChanges();
                        PopGridView();
                        ClearFields();
                        btnSave.Text = "Save";

                    }
                }
            }
        }

        void ClearFields() {
            txtComicName.Text = "";
            txtComicPublisher.Text = "";
            txtComicVolume.Text = "";
            txtComicIssueNumber.Text = "";
            txtDatePublished.Text = "";
            txtComicPrice.Text = "";
        }
    }
}
